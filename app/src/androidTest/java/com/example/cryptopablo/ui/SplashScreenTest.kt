package com.example.cryptopablo.ui

import android.content.Context
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.MediumTest
import com.example.cryptopablo.R
import com.example.cryptopablo.ui.main.splash.SplashFragment
import com.example.cryptopablo.utils.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.File

@MediumTest
@HiltAndroidTest
class SplashScreenTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun setup() {
        hiltRule.inject()
    }

    @After
    fun teardown() {
        File(
            ApplicationProvider.getApplicationContext<Context>().filesDir,
            "datastore"
        ).deleteRecursively()
    }

    @Test
    fun testSplashIsShown() {
        launchFragmentInHiltContainer<SplashFragment>()
        runBlocking { delay(1000) }

        onView(withId(R.id.splash_animation)).check(matches(isDisplayed()))
    }

    @Test
    fun testSplashIsNotShownAfterFiveSeconds() {
        val navController = TestNavHostController(ApplicationProvider.getApplicationContext())
        launchFragment(navController)
        runBlocking { delay(5000) }

        assertNotEquals(navController.currentDestination?.id, R.id.splashFragment)
    }

    @Test
    fun testNavigationToLoginScreen() {
        val navController = TestNavHostController(ApplicationProvider.getApplicationContext())

        launchFragment(navController)
        runBlocking { delay(5000) }

        assertEquals(navController.currentDestination?.id, R.id.loginFragment)
    }

    @Test
    fun testNavigationToLoginScreenWhenTimeIsMinorToFourSeconds() {
        val navController = TestNavHostController(ApplicationProvider.getApplicationContext())

        launchFragment(navController)

        runBlocking { delay(3000) }
        assertNotEquals(navController.currentDestination?.id, R.id.loginFragment)
    }

    private fun launchFragment(navController: NavController) {
        launchFragmentInHiltContainer<SplashFragment> {
            navController.setGraph(R.navigation.nav_graph)
            Navigation.setViewNavController(requireView(), navController)
        }
    }
}
