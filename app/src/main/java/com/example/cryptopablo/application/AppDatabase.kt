package com.example.cryptopablo.application

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.cryptopablo.data.local.CryptoDao
import com.example.cryptopablo.data.models.latest.CryptoMetadataEntity

@Database(entities = [CryptoMetadataEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun cryptoDao(): CryptoDao
}
