package com.example.cryptopablo.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.cryptopablo.data.models.latest.CryptoMetadataEntity

@Dao
interface CryptoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveCrypto(crypto: CryptoMetadataEntity)

    @Query("SELECT * FROM cryptometadataentity")
    suspend fun getAllCryptos(): List<CryptoMetadataEntity>
}
