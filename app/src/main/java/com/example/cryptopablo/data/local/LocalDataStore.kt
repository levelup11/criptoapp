package com.example.cryptopablo.data.local

interface LocalDataStore {

    suspend fun saveUserName(username: String)

    suspend fun savePassword(password: String)

    suspend fun loadUserName(): String

    suspend fun loadPassword(): String
}
