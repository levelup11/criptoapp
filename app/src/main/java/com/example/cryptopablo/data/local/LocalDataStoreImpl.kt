package com.example.cryptopablo.data.local

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import com.example.cryptopablo.application.AppConstants.PASSWORD_KEY
import com.example.cryptopablo.application.AppConstants.USER_NAME_KEY
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class LocalDataStoreImpl @Inject constructor(private val dataStore: DataStore<Preferences>) :
    LocalDataStore {

    companion object {
        val USERNAME = stringPreferencesKey(USER_NAME_KEY)
        val PASSWORD = stringPreferencesKey(PASSWORD_KEY)
    }

    override suspend fun saveUserName(username: String) {
        dataStore.edit { settings ->
            settings[USERNAME] = username
        }
    }

    override suspend fun savePassword(password: String) {
        dataStore.edit { settings -> settings[PASSWORD] = password }
    }

    override suspend fun loadUserName(): String = dataStore.data.map {
            preferences -> preferences[USERNAME] ?: ""
    }.first()

    override suspend fun loadPassword(): String = dataStore.data.map {
            preferences -> preferences[PASSWORD] ?: ""
    }.first()
}
