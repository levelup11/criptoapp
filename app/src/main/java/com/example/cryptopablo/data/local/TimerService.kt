package com.example.cryptopablo.data.local

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.widget.Toast
import com.example.cryptopablo.R
import com.example.cryptopablo.application.AppConstants.A_SECOND
import com.example.cryptopablo.application.AppConstants.BREAK_TIME
import com.example.cryptopablo.application.AppConstants.TIMER_UPDATED
import com.example.cryptopablo.application.AppConstants.TIME_EXTRA
import com.example.cryptopablo.managers.RefreshDataManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import java.util.Timer
import java.util.TimerTask
import javax.inject.Inject

@AndroidEntryPoint
class TimerService : Service() {

    @Inject
    lateinit var refreshDataManager: RefreshDataManager
    private val job = SupervisorJob()
    private val scope = CoroutineScope(Dispatchers.IO + job)
    private val mainScope = CoroutineScope(Dispatchers.Main + job)

    override fun onBind(p0: Intent?): IBinder? = null

    private val timer = Timer()

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val time = intent.getDoubleExtra(TIME_EXTRA, 0.0)
        timer.scheduleAtFixedRate(TimeTask(time), 0, A_SECOND)
        return START_NOT_STICKY
    }

    private inner class TimeTask(private var time: Double) : TimerTask() {
        override fun run() {
            val intent = Intent(TIMER_UPDATED)
            time++
            intent.putExtra(TIME_EXTRA, time)
            sendBroadcast(intent)
            if (time == BREAK_TIME) scope.launch {
                val correctRefresh = refreshDataManager.refreshDB()
                checkCorrectRefresh(correctRefresh)
                timer.cancel()
            }
        }
    }

    private fun checkCorrectRefresh(correctRefresh: Boolean) {
        if (!correctRefresh) {
            mainScope.launch {
                Toast.makeText(this@TimerService.applicationContext,
                    applicationContext.getString(R.string.error), Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        timer.cancel()
        job.cancel()
    }
}
