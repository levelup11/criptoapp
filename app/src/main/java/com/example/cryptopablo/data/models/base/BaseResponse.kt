package com.example.cryptopablo.data.models.base

import com.example.cryptopablo.data.models.latest.StatusResponse
import com.google.gson.annotations.SerializedName

class BaseResponse<T> (
    val status: StatusResponse? = null,
    @SerializedName("data") val data: List<T>? = null
)
