package com.example.cryptopablo.data.models.base

enum class LocalDBStates {
    SAVED,
    NOT_SAVED,
    ERROR
}
