package com.example.cryptopablo.data.models.details

import com.example.cryptopablo.data.models.latest.StatusResponse
import com.google.gson.annotations.SerializedName

data class DetailResponse<T> (
    val status: StatusResponse? = null,
    @SerializedName("data") val data: T? = null
)
