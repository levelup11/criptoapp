package com.example.cryptopablo.data.models.latest

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CryptoMetadataEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val name: String = "",
    val symbol: String = "",
    val price: Double = 0.0,
    val marketCap: Double = 0.0,
    val lastUpdated: String = ""
)
