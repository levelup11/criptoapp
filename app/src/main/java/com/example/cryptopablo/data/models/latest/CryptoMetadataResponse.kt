package com.example.cryptopablo.data.models.latest

import com.google.gson.annotations.SerializedName

data class CryptoMetadataResponse(
    @SerializedName("id") val id: Int? = null,
    @SerializedName("name") val name: String? = null,
    @SerializedName("slug") val slug: String? = null,
    @SerializedName("symbol") val symbol: String? = null,
    @SerializedName("circulating_supply") val circulatingSupply: Double? = null,
    @SerializedName("cmc_rank") val cmcRank: Int? = null,
    @SerializedName("quote") val quote: Quote? = null,
    @SerializedName("date_added") val dateAdded: String? = null,
    @SerializedName("last_updated") val lastUpdated: String? = null,
    @SerializedName("max_supply") val maxSupply: Double? = null,
    @SerializedName("num_market_pairs") val numMarketPairs: Int? = null,
    @SerializedName("tags") val tags: List<String>? = null,
    @SerializedName("total_supply") val totalSupply: Double? = null
)
