package com.example.cryptopablo.data.models.latest

import com.google.gson.annotations.SerializedName

data class StatusResponse(
    @SerializedName("credit_count") val creditCount: Int? = null,
    @SerializedName("elapsed") val elapsed: Int? = null,
    @SerializedName("error_code") val errorCode: Int? = null,
    @SerializedName("error_message") val errorMessage: String? = null,
    @SerializedName("timestamp") val timestamp: String? = null,
    @SerializedName("total_count") val totalCount: Int? = null
)
