package com.example.cryptopablo.data.remote

import com.example.cryptopablo.data.models.base.BaseResponse
import com.example.cryptopablo.data.models.details.DetailResponse
import com.example.cryptopablo.data.models.latest.CryptoMetadataResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface CryptoWebService {

    @GET("v1/cryptocurrency/listings/latest?limit=50")
    suspend fun getAllCoins(): BaseResponse<CryptoMetadataResponse>

    @GET("v2/cryptocurrency/info")
    suspend fun getCoinDetail(@Query("id") id: Int): DetailResponse<Any>
}
