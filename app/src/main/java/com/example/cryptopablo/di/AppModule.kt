package com.example.cryptopablo.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.room.Room
import com.example.cryptopablo.BuildConfig
import com.example.cryptopablo.application.AppDatabase
import com.example.cryptopablo.application.InternetCheck
import com.example.cryptopablo.data.local.LocalDataStore
import com.example.cryptopablo.data.local.LocalDataStoreImpl
import com.example.cryptopablo.data.remote.CryptoWebService
import com.example.cryptopablo.di.interceptors.CryptoInterceptor
import com.example.cryptopablo.managers.WorkerManager
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun providesLocalDataStoreSource(localDataStore: DataStore<Preferences>): LocalDataStore =
        LocalDataStoreImpl(localDataStore)

    @Provides
    fun provideLoggInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    @Provides
    fun provideCryptoInterceptor(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
        OkHttpClient.Builder().apply {
            addInterceptor(CryptoInterceptor())
            addInterceptor(loggingInterceptor)
        }.build()

    @Singleton
    @Provides
    fun providesRetrofit(client: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()

    @Provides
    fun providesWebService(retrofit: Retrofit): CryptoWebService =
        retrofit.create(CryptoWebService::class.java)

    @Singleton
    @Provides
    fun providesRoom(@ApplicationContext context: Context) =
        Room.databaseBuilder(
            context, AppDatabase::class.java,
            "cryptoDB"
        ).build()

    @Provides
    fun providesDao(db: AppDatabase) = db.cryptoDao()

    @Provides
    fun provideInternetChecker(@ApplicationContext context: Context) = InternetCheck(context)

    @Singleton
    @Provides
    fun provideWorkerManager(@ApplicationContext context: Context): WorkerManager {
        return WorkerManager(context)
    }
}
