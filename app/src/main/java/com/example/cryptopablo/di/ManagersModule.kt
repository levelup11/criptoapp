package com.example.cryptopablo.di

import com.example.cryptopablo.managers.LoginManager
import com.example.cryptopablo.managers.LoginManagerImpl
import com.example.cryptopablo.repository.LocalRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped

@InstallIn(ActivityRetainedComponent::class)
@Module
class ManagersModule {

    @Provides
    @ActivityRetainedScoped
    fun providesValidatorImpl(localRepository: LocalRepository): LoginManager {
        return LoginManagerImpl(localRepository)
    }
}
