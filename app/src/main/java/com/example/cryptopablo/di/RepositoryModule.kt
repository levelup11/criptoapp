package com.example.cryptopablo.di

import com.example.cryptopablo.application.InternetCheck
import com.example.cryptopablo.data.local.CryptoDao
import com.example.cryptopablo.data.local.LocalDataStore
import com.example.cryptopablo.data.remote.CryptoWebService
import com.example.cryptopablo.managers.detail.ParseDetailManagerImpl
import com.example.cryptopablo.managers.detail.ParserDetailManager
import com.example.cryptopablo.repository.LocalRepository
import com.example.cryptopablo.repository.LocalRepositoryImpl
import com.example.cryptopablo.repository.MainRepository
import com.example.cryptopablo.repository.MainRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    fun providesLocalRepository(localDataStore: LocalDataStore): LocalRepository {
        return LocalRepositoryImpl(localDataStore)
    }

    @Provides
    fun providesRemoteRepository(
        webService: CryptoWebService,
        dao: CryptoDao,
        internetCheck: InternetCheck
    ): MainRepository = MainRepositoryImpl(webService, dao, internetCheck)

    @Provides
    fun providesParserDetailManager(repository: MainRepository): ParserDetailManager {
        return ParseDetailManagerImpl(repository)
    }
}
