package com.example.cryptopablo.di

import com.example.cryptopablo.managers.RefreshDataManager
import com.example.cryptopablo.managers.RefreshDataManagerImpl
import com.example.cryptopablo.repository.MainRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ServiceComponent

@Module
@InstallIn(ServiceComponent::class)
class ServicesModule {

    @Provides
    fun providesRefreshDataManager(repository: MainRepository): RefreshDataManager {
        return RefreshDataManagerImpl(repository)
    }
}
