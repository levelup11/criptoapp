package com.example.cryptopablo.managers

interface LoginManager {

    fun validateUserInputs(username: String, password: String): Boolean

    suspend fun compareUserCredentials(username: String, password: String): Boolean
}
