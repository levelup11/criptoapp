package com.example.cryptopablo.managers

import com.example.cryptopablo.application.AppConstants.PASSWORD_MIN_LENGTH
import com.example.cryptopablo.application.AppConstants.USERNAME_MIN_LENGTH
import com.example.cryptopablo.repository.LocalRepository
import javax.inject.Inject

class LoginManagerImpl @Inject constructor(private val localRepository: LocalRepository) :
    LoginManager {

    override fun validateUserInputs(username: String, password: String): Boolean {
        return username.isNotEmpty() && username.length > USERNAME_MIN_LENGTH &&
                password.isNotEmpty() && password.length > PASSWORD_MIN_LENGTH
    }

    override suspend fun compareUserCredentials(username: String, password: String): Boolean =
        localRepository.loadUserName() == username && localRepository.loadPassword() == password
}
