package com.example.cryptopablo.managers

interface RefreshDataManager {

    suspend fun refreshDB(): Boolean
}
