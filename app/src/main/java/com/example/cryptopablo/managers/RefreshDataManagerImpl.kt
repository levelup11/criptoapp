package com.example.cryptopablo.managers

import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import com.example.cryptopablo.repository.MainRepository
import okio.IOException
import retrofit2.HttpException
import javax.inject.Inject

class RefreshDataManagerImpl @Inject constructor(private val repository: MainRepository) :
    RefreshDataManager {

    override suspend fun refreshDB(): Boolean {
        return try {
            repository.refreshDataBase()
            true
        } catch (e: SQLiteConstraintException) {
            Log.d("Error", e.message.toString())
            false
        } catch (e: IOException) {
            Log.d("Error", e.message.toString())
            false
        } catch (e: HttpException) {
            Log.d("Error", e.message.toString())
            false
        }
    }
}
