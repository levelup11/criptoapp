package com.example.cryptopablo.managers

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.example.cryptopablo.repository.MainRepository
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import okio.IOException
import retrofit2.HttpException

@HiltWorker
class RefreshDataWorker @AssistedInject constructor(
    @Assisted context: Context,
    @Assisted workerParams: WorkerParameters,
    private val repositoryImpl: MainRepository
) : CoroutineWorker(context, workerParams) {

    override suspend fun doWork(): Result {
        return try {
            if (repositoryImpl.getAllCryptos().isNotEmpty()) repositoryImpl.refreshDataBase()
            Result.success()
        } catch (e: SQLiteConstraintException) {
            Log.d("Error", e.message.toString())
            Result.success()
        } catch (e: IOException) {
            Log.d("Error", e.message.toString())
            Result.success()
        } catch (e: HttpException) {
            Log.d("Error", e.message.toString())
            Result.success()
        }
    }

    companion object {
        const val WORK_NAME = "com.example.cryptopablo.managers.RefreshDataWorker"
    }
}
