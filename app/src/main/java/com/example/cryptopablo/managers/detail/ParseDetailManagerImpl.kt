package com.example.cryptopablo.managers.detail

import com.example.cryptopablo.data.models.details.CoinDetail
import com.example.cryptopablo.repository.MainRepository
import com.google.gson.Gson
import org.json.JSONObject
import javax.inject.Inject

class ParseDetailManagerImpl @Inject constructor(private val repository: MainRepository) :
    ParserDetailManager {

    override suspend fun parseData(id: Int): CoinDetail? {
        with(Gson()) {
            val json = toJson(repository.getCryptoDetails(id).data)
            val jsonObject = JSONObject(json).getJSONObject(id.toString())
            return fromJson(jsonObject.toString(), CoinDetail::class.java)
        }
    }
}
