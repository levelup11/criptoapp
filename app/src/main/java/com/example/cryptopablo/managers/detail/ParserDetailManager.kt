package com.example.cryptopablo.managers.detail

import com.example.cryptopablo.data.models.details.CoinDetail

interface ParserDetailManager {

    suspend fun parseData(id: Int): CoinDetail?
}
