package com.example.cryptopablo.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.cryptopablo.managers.LoginManager
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val loginManager: LoginManager) : ViewModel() {

    private val _areFieldsValid = MutableLiveData(false)
    val areFieldsValid: LiveData<Boolean>
        get() = _areFieldsValid

    fun validateFields(username: String, password: String) {
        _areFieldsValid.value = loginManager.validateUserInputs(username, password)
    }

    fun login(username: String, password: String) = liveData<Boolean> {
        emit(loginManager.compareUserCredentials(username, password))
    }
}
