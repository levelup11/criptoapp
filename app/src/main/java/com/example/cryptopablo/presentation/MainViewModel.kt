package com.example.cryptopablo.presentation

import android.database.sqlite.SQLiteConstraintException
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.example.cryptopablo.data.models.base.Response
import com.example.cryptopablo.managers.WorkerManager
import com.example.cryptopablo.managers.detail.ParserDetailManager
import com.example.cryptopablo.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okio.IOException
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val remoteRepository: MainRepository,
    private val workerManager: WorkerManager,
    private val parserDetailManager: ParserDetailManager
) : ViewModel() {

    fun refreshCryptos() {
        viewModelScope.launch(viewModelScope.coroutineContext + Dispatchers.IO) {
            workerManager.refreshCryptos()
        }
    }

    fun fetchAllCryptoCoins() = liveData(viewModelScope.coroutineContext + Dispatchers.IO) {
        emit(Response.Loading())
        try {
            val listOfCryptos = remoteRepository.getAllCryptos()
            if (listOfCryptos.isNotEmpty()) {
                emit(Response.Success(listOfCryptos))
            } else {
                emit(Response.Success(remoteRepository.getDataForFirstTime()))
            }
        } catch (e: SQLiteConstraintException) {
            emit(Response.Failure(e))
        } catch (e: IOException) {
            emit(Response.Failure(e))
        } catch (e: HttpException) {
            emit(Response.Failure(e))
        }
    }

    fun getCoinDetail(id: Int) = liveData(viewModelScope.coroutineContext + Dispatchers.IO) {
        emit(Response.Loading())
        try {
            emit(Response.Success(parserDetailManager.parseData(id)))
        } catch (e: SQLiteConstraintException) {
            emit(Response.Failure(e))
        } catch (e: IOException) {
            emit(Response.Failure(e))
        } catch (e: HttpException) {
            emit(Response.Failure(e))
        }
    }
}
