package com.example.cryptopablo.presentation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cryptopablo.data.models.base.LocalDBStates
import com.example.cryptopablo.repository.LocalRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okio.IOException
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(private val localRepository: LocalRepository) :
    ViewModel() {

    private val _saveIsSuccess = MutableLiveData<LocalDBStates>()
    val saveIsSuccess: LiveData<LocalDBStates>
        get() = _saveIsSuccess

    fun saveHardCodedData() {
        try {
            viewModelScope.launch(viewModelScope.coroutineContext + Dispatchers.IO) {
                if (checkIfUserIsEmpty()) {
                    localRepository.saveUserName("Pepito47")
                    localRepository.savePassword("123456")
                    _saveIsSuccess.postValue(LocalDBStates.SAVED)
                } else {
                    _saveIsSuccess.postValue(LocalDBStates.NOT_SAVED)
                }
            }
        } catch (e: IOException) {
            Log.d("Error", e.message.toString())
            _saveIsSuccess.postValue(LocalDBStates.ERROR)
        }
    }

    private suspend fun checkIfUserIsEmpty(): Boolean {
        return localRepository.loadUserName().isEmpty() ||
                localRepository.loadPassword().isEmpty()
    }
}
