package com.example.cryptopablo.repository

interface LocalRepository {

    suspend fun saveUserName(username: String)

    suspend fun savePassword(password: String)

    suspend fun loadUserName(): String

    suspend fun loadPassword(): String
}
