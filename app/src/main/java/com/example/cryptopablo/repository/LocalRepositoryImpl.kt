package com.example.cryptopablo.repository

import com.example.cryptopablo.data.local.LocalDataStore
import javax.inject.Inject

class LocalRepositoryImpl @Inject constructor(private val localDataStore: LocalDataStore) :
    LocalRepository {

    override suspend fun saveUserName(username: String) {
        localDataStore.saveUserName(username)
    }

    override suspend fun savePassword(password: String) {
        localDataStore.savePassword(password)
    }

    override suspend fun loadUserName(): String = localDataStore.loadUserName()

    override suspend fun loadPassword(): String = localDataStore.loadPassword()
}
