package com.example.cryptopablo.repository

import com.example.cryptopablo.data.models.details.DetailResponse
import com.example.cryptopablo.data.models.latest.CryptoMetadataEntity

interface MainRepository {

    suspend fun getDataForFirstTime(): List<CryptoMetadataEntity>

    suspend fun refreshDataBase()

    suspend fun getAllCryptos(): List<CryptoMetadataEntity>

    suspend fun getCryptoDetails(id: Int): DetailResponse<Any>
}
