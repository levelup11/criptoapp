package com.example.cryptopablo.repository

import com.example.cryptopablo.application.InternetCheck
import com.example.cryptopablo.application.toCryptoEntity
import com.example.cryptopablo.data.local.CryptoDao
import com.example.cryptopablo.data.models.details.DetailResponse
import com.example.cryptopablo.data.models.latest.CryptoMetadataEntity
import com.example.cryptopablo.data.remote.CryptoWebService
import javax.inject.Inject

class MainRepositoryImpl @Inject constructor(
    private val webService: CryptoWebService,
    private val dao: CryptoDao,
    private val internetCheck: InternetCheck
) : MainRepository {

    override suspend fun getDataForFirstTime(): List<CryptoMetadataEntity> {
        return if (internetCheck.isNetworkAvailable()) {
            saveCryptosInCache()
            dao.getAllCryptos()
        } else {
            dao.getAllCryptos()
        }
    }

    override suspend fun refreshDataBase() {
        saveCryptosInCache()
    }

    override suspend fun getAllCryptos(): List<CryptoMetadataEntity> = dao.getAllCryptos()

    override suspend fun getCryptoDetails(id: Int): DetailResponse<Any> {
        return webService.getCoinDetail(id)
    }

    private suspend fun saveCryptosInCache() {
        webService.getAllCoins().data?.forEach { crypto ->
            dao.saveCrypto(crypto.toCryptoEntity())
        }
    }
}
