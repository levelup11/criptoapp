package com.example.cryptopablo.ui.main

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.example.cryptopablo.R
import com.example.cryptopablo.application.AppConstants.TIMER_UPDATED
import com.example.cryptopablo.application.AppConstants.TIME_EXTRA
import com.example.cryptopablo.data.local.TimerService
import com.example.cryptopablo.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private lateinit var serviceIntent: Intent
    private var time = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initNavController()
        serviceIntent = Intent(applicationContext, TimerService::class.java)
        registerReceiver(updateTime, IntentFilter(TIMER_UPDATED))
    }

    override fun onPause() {
        super.onPause()
        startTimer()
    }

    override fun onResume() {
        super.onResume()
        stopTimer()
    }

    private fun initNavController() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fragment_container_view) as NavHostFragment
        navController = navHostFragment.navController
    }

    private fun startTimer() {
        serviceIntent.putExtra(TIME_EXTRA, time)
        startService(serviceIntent)
    }

    private fun stopTimer() {
        time = 0.0
        if (::serviceIntent.isInitialized) stopService(serviceIntent)
    }

    private val updateTime: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            time = intent.getDoubleExtra(TIME_EXTRA, 0.0)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        stopService(serviceIntent)
    }
}
