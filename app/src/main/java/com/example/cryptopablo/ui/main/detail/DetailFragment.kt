package com.example.cryptopablo.ui.main.detail

import android.content.res.ColorStateList
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.cryptopablo.R
import com.example.cryptopablo.application.hide
import com.example.cryptopablo.application.show
import com.example.cryptopablo.application.showSnackError
import com.example.cryptopablo.data.models.base.Response
import com.example.cryptopablo.data.models.details.CoinDetail
import com.example.cryptopablo.databinding.FragmentDetailBinding
import com.example.cryptopablo.presentation.MainViewModel
import com.google.android.material.chip.Chip
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : Fragment(R.layout.fragment_detail) {

    private lateinit var binding: FragmentDetailBinding
    private val args by navArgs<DetailFragmentArgs>()

    private val mainViewModel by viewModels<MainViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentDetailBinding.bind(view)
        getCryptoData()
    }

    private fun getCryptoData() {
        mainViewModel.getCoinDetail(args.id).observe(viewLifecycleOwner) { response ->
            when (response) {
                is Response.Loading -> {
                    binding.loadingAnimation.show()
                }
                is Response.Success -> {
                    binding.loadingAnimation.hide()
                    response.data?.let { coinDetail -> drawData(coinDetail) }
                }
                is Response.Failure -> {
                    showSnackError(response.e.message.toString())
                }
            }
        }
    }

    private fun drawData(coinDetail: CoinDetail) {
        with(binding) {
            tvTitle.text = coinDetail.name
            tvDescription.text = coinDetail.description
            tvSymbolDetail.text = coinDetail.symbol
            tvPrice.text = args.price
            tvLastUpdate.text = getString(R.string.last_update, args.lastUpdated)
        }
        loadImage(coinDetail.logo)
        coinDetail.tags.forEach { tag -> addChip(tag) }
    }

    private fun loadImage(imagePath: String) {
        Glide
            .with(this)
            .load(imagePath)
            .placeholder(R.mipmap.ic_launcher_round)
            .into(binding.imgBackground)
    }

    private fun addChip(genre: String) {
        val chip = Chip(requireContext())
        with(chip) {
            text = genre
            typeface = Typeface.DEFAULT_BOLD
            isClickable = false
            chipBackgroundColor =
                ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.gray))
            setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        }
        binding.tagContainer.addView(chip)
    }
}
