package com.example.cryptopablo.ui.main.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.cryptopablo.R
import com.example.cryptopablo.application.formatDate
import com.example.cryptopablo.application.roundOffDecimal
import com.example.cryptopablo.data.models.latest.CryptoMetadataEntity
import com.example.cryptopablo.databinding.ItemCryptoBinding

class CryptoAdapter(private val callBack: OnCryptoClick) :
    RecyclerView.Adapter<CryptoAdapter.CryptoViewHolder>() {

    interface OnCryptoClick {
        fun onButtonClick(cryptoItem: CryptoMetadataEntity)
    }

    private lateinit var context: Context
    private var cryptoList: MutableList<CryptoMetadataEntity> = mutableListOf()

    fun updateData(cryptoList: MutableList<CryptoMetadataEntity>) {
        val diffUtil = CryptoDiffUtils(this.cryptoList, cryptoList)
        val diffResult = DiffUtil.calculateDiff(diffUtil)
        this.cryptoList = cryptoList
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CryptoViewHolder {
        context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        return CryptoViewHolder(layoutInflater.inflate(R.layout.item_crypto, parent, false))
    }

    override fun onBindViewHolder(holder: CryptoViewHolder, position: Int) {
        val animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
        val item = cryptoList[position]
        with(holder.binding) {
            tvName.text = item.name
            tvSymbol.text = item.symbol
            tvAmount.text = item.price.roundOffDecimal()
            tvLastUpdate.text =
                tvLastUpdate.context.getString(R.string.last_update, item.lastUpdated.formatDate())
            btnDetails.setOnClickListener { callBack.onButtonClick(item) }
        }
        holder.itemView.startAnimation(animation)
    }

    override fun getItemCount(): Int = cryptoList.size

    inner class CryptoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding: ItemCryptoBinding = ItemCryptoBinding.bind(view)
    }
}
