package com.example.cryptopablo.ui.main.home

import androidx.recyclerview.widget.DiffUtil
import com.example.cryptopablo.data.models.latest.CryptoMetadataEntity

class CryptoDiffUtils(
    private val oldList: MutableList<CryptoMetadataEntity>,
    private val newList: MutableList<CryptoMetadataEntity>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id &&
                oldList[oldItemPosition].name == newList[newItemPosition].name &&
                oldList[oldItemPosition].symbol == newList[newItemPosition].symbol &&
                oldList[oldItemPosition].price == newList[newItemPosition].price
    }
}
