package com.example.cryptopablo.ui.main.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.cryptopablo.R
import com.example.cryptopablo.application.showSnackError
import com.example.cryptopablo.application.whenBackPressed
import com.example.cryptopablo.application.show
import com.example.cryptopablo.application.hide
import com.example.cryptopablo.application.roundOffDecimal
import com.example.cryptopablo.application.formatDate
import com.example.cryptopablo.data.models.base.Response
import com.example.cryptopablo.data.models.latest.CryptoMetadataEntity
import com.example.cryptopablo.databinding.FragmentHomeBinding
import com.example.cryptopablo.presentation.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment(R.layout.fragment_home), CryptoAdapter.OnCryptoClick {

    private lateinit var binding: FragmentHomeBinding
    private val cryptoAdapter by lazy { CryptoAdapter(this) }

    private val mainViewModel by viewModels<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainViewModel.refreshCryptos()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentHomeBinding.bind(view)
        setupRecyclerView()
        getCryptoInfo()
        requireActivity().whenBackPressed(viewLifecycleOwner)
    }

    private fun getCryptoInfo() {
        mainViewModel.fetchAllCryptoCoins().observe(viewLifecycleOwner) { response ->
            when (response) {
                is Response.Loading -> {
                    showLoading()
                }
                is Response.Success -> {
                    showScreen()
                    feedRecyclerView(response.data)
                }
                is Response.Failure -> {
                    showSnackError(response.e.message.toString())
                }
            }
        }
    }

    private fun showScreen() {
        with(binding) {
            loadingAnimation.hide()
            rvCrypto.show()
            tvTitle.show()
        }
    }

    private fun showLoading() {
        with(binding) {
            loadingAnimation.show()
            rvCrypto.hide()
            tvTitle.hide()
        }
    }

    private fun setupRecyclerView() {
        binding.rvCrypto.adapter = cryptoAdapter
    }

    private fun feedRecyclerView(data: List<CryptoMetadataEntity>) {
        if (data.isNotEmpty()) {
            cryptoAdapter.updateData(data.sortedByDescending { it.price }.toMutableList())
        } else {
            showEmptyAnimation()
        }
    }

    private fun showEmptyAnimation() {
        binding.emptyAnimation.show()
    }

    override fun onButtonClick(cryptoItem: CryptoMetadataEntity) {
        val action = HomeFragmentDirections
            .actionHomeFragmentToDetailFragment(
                cryptoItem.id,
                cryptoItem.price.roundOffDecimal(),
                cryptoItem.lastUpdated.formatDate()
            )
        findNavController().navigate(action)
    }
}
