package com.example.cryptopablo.ui.main.login

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.cryptopablo.R
import com.example.cryptopablo.application.AppConstants.FAKE_LOADING_TIME
import com.example.cryptopablo.application.hideKeyboard
import com.example.cryptopablo.application.showSnackError
import com.example.cryptopablo.application.whenBackPressed
import com.example.cryptopablo.databinding.FragmentLoginBinding
import com.example.cryptopablo.presentation.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class LoginFragment : Fragment(R.layout.fragment_login) {

    private lateinit var binding: FragmentLoginBinding
    private val loginViewModel by viewModels<LoginViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentLoginBinding.bind(view)
        setupObservers()
        listenFields()
        setupButtons()
        requireActivity().whenBackPressed(viewLifecycleOwner)
    }

    private fun setupObservers() {
        loginViewModel.areFieldsValid.observe(viewLifecycleOwner) { areValidFields ->
            if (areValidFields) enableButton() else disableButton()
        }
    }

    private fun listenFields() {
        with(binding) {
            etUsername.doAfterTextChanged { captureValues() }
            etPassword.doAfterTextChanged { captureValues() }
        }
    }

    private fun setupButtons() {
        binding.btnLogin.setOnClickListener {
            val username = binding.etUsername.text.toString()
            val password = binding.etPassword.text.toString()

            login(username, password)
            requireView().hideKeyboard()
        }
    }

    private fun login(username: String, password: String) {
        loginViewModel.login(username, password).observe(viewLifecycleOwner) { isValidUser ->
            if (isValidUser) simulateLogin()
            else showSnackError("Wrong username or password")
        }
    }

    private fun captureValues() {
        val username = binding.etUsername.text.toString()
        val password = binding.etPassword.text.toString()
        loginViewModel.validateFields(username, password)
    }

    private fun enableButton() {
        with(binding) {
            btnLogin.isEnabled = true
            btnLogin.setBackgroundColor(
                ContextCompat.getColor(requireContext(), R.color.crypto_green)
            )
        }
    }

    private fun disableButton() {
        with(binding) {
            btnLogin.isEnabled = false
            btnLogin.setBackgroundColor(Color.LTGRAY)
        }
    }

    private fun simulateLogin() {
        binding.progressBar.isVisible = true
        lifecycleScope.launch {
            delay(FAKE_LOADING_TIME)
            findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
        }
    }
}
