package com.example.cryptopablo.ui.main.splash

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.cryptopablo.R
import com.example.cryptopablo.application.AppConstants.SPLASH_TIME
import com.example.cryptopablo.application.toast
import com.example.cryptopablo.data.models.base.LocalDBStates.NOT_SAVED
import com.example.cryptopablo.data.models.base.LocalDBStates.SAVED
import com.example.cryptopablo.presentation.SplashViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SplashFragment : Fragment(R.layout.fragment_splash) {

    private val splashViewModel by viewModels<SplashViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSplash()
    }

    private fun setupSplash() {
        lifecycleScope.launch {
            splashViewModel.saveHardCodedData()
            delay(SPLASH_TIME)
            observeSavedData()
        }
    }

    private fun observeSavedData() {
        splashViewModel.saveIsSuccess.observe(viewLifecycleOwner) { savedState ->
            when (savedState) {
                SAVED, NOT_SAVED -> navigateToMain()
                else -> toast(getString(R.string.error))
            }
        }
    }

    private fun navigateToMain() {
        findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
    }
}
