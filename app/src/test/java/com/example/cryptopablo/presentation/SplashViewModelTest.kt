package com.example.cryptopablo.presentation

import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.cryptopablo.data.models.base.LocalDBStates
import com.example.cryptopablo.repository.LocalRepository
import com.example.cryptopablo.util.MainCoroutineRule
import com.example.cryptopablo.util.PASSWORD
import com.example.cryptopablo.util.USER_NAME
import com.example.cryptopablo.util.getOrAwaitValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Assert.assertThrows
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.doThrow
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class SplashViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineRule = MainCoroutineRule()

    private val localRepositoryMock: LocalRepository = mock()
    private lateinit var splashViewModel: SplashViewModel

    @Before
    fun setUp() {
        splashViewModel = SplashViewModel(localRepositoryMock)
    }

    @Test
    fun saveHardCodedData_TriesToSaveData_ReturnsSaved() = runTest {

        whenever(localRepositoryMock.loadUserName()).doReturn("")
        whenever(localRepositoryMock.loadPassword()).doReturn("")

        splashViewModel.saveHardCodedData()

        assertEquals(LocalDBStates.SAVED, splashViewModel.saveIsSuccess.getOrAwaitValue())
    }

    @Test
    fun `given filled data in repository shouldn't return State SAVED`() = runTest {

        whenever(localRepositoryMock.loadUserName()).doReturn(USER_NAME)
        whenever(localRepositoryMock.loadPassword()).doReturn(PASSWORD)

        splashViewModel.saveHardCodedData()

        assertNotEquals(LocalDBStates.SAVED, splashViewModel.saveIsSuccess.getOrAwaitValue())
    }

    @Test
    fun saveHardCodedData_TriesToSaveData_ReturnsNOTSaved() = runTest {

        whenever(localRepositoryMock.loadUserName()).doReturn(USER_NAME)
        whenever(localRepositoryMock.loadPassword()).doReturn(PASSWORD)

        splashViewModel.saveHardCodedData()

        assertEquals(LocalDBStates.NOT_SAVED, splashViewModel.saveIsSuccess.getOrAwaitValue())
    }

    @Test
    fun saveHardCodedData_TriesToSaveData_ReturnsError() = runTest {

        whenever(localRepositoryMock.loadUserName()).doReturn("")
        whenever(localRepositoryMock.loadPassword()).doReturn("")

        splashViewModel.saveHardCodedData()

        assertNotEquals(LocalDBStates.NOT_SAVED, splashViewModel.saveIsSuccess.getOrAwaitValue())
    }

    @Test
    fun saveHardCodedData_launchesAnException_ReturnsError() = runTest {

        whenever(localRepositoryMock.saveUserName(USER_NAME)).thenReturn(null)
        whenever(localRepositoryMock.savePassword(PASSWORD)).thenReturn(null)

        try {
            splashViewModel.saveHardCodedData()
        } catch (e: Exception) {
            Log.d("Error", e.message.toString())
            assertEquals(LocalDBStates.ERROR, splashViewModel.saveIsSuccess.getOrAwaitValue())
        }
    }

    @Test(expected = Exception::class)
    fun saveHardCodedData_LaunchesAnException_CatchByBlock() = runTest {

        whenever(localRepositoryMock.saveUserName(USER_NAME)).doThrow(Exception())
        whenever(localRepositoryMock.savePassword(PASSWORD)).doThrow(Exception())

        assertThrows(Exception::class.java) { splashViewModel.saveHardCodedData() }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}
