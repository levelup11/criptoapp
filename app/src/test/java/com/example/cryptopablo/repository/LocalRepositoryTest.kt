package com.example.cryptopablo.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.cryptopablo.data.local.LocalDataStore
import com.example.cryptopablo.util.MainCoroutineRule
import com.example.cryptopablo.util.PASSWORD
import com.example.cryptopablo.util.USER_NAME
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class LocalRepositoryTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineRule = MainCoroutineRule()

    private val localStorageSource: LocalDataStore = mock()
    private lateinit var localRepository: LocalRepositoryImpl

    @Before
    fun setup() {
        localRepository = LocalRepositoryImpl(localStorageSource)
    }

    @Test
    fun saveUserName_SavesData_IsExecuted() = runTest {
        localRepository.saveUserName(USER_NAME)
        verify(localStorageSource, times(1)).saveUserName(USER_NAME)
    }

    @Test
    fun saveUserName_SavesData_IsExecuted_WithEmptyArg() = runTest {
        localRepository.saveUserName("")
        verify(localStorageSource, times(1)).saveUserName("")
    }

    @Test
    fun savePassword_SavesData_IsExecuted() = runTest {
        localRepository.savePassword(PASSWORD)
        verify(localStorageSource, times(1)).savePassword(PASSWORD)
    }
    @Test
    fun savePassword_SavesData_IsExecutedWithEmptyArg() = runTest {
        localRepository.savePassword("")
        verify(localStorageSource, times(1)).savePassword("")
    }

    @Test
    fun saveUserName_SavesData_ReturnsNotNotEmptyValue() = runTest {
        localRepository.saveUserName(USER_NAME)
        whenever(localStorageSource.loadUserName()).doReturn(USER_NAME)

        Assert.assertNotEquals("", localRepository.loadUserName())
    }

    @Test
    fun saveUserName_SavesData_ReturnsTheSameValueInLoadFunction() = runTest {
        localRepository.saveUserName(USER_NAME)
        whenever(localStorageSource.loadUserName()).doReturn(USER_NAME)

        Assert.assertEquals(USER_NAME, localRepository.loadUserName())
    }

    @Test
    fun savePassword_SavesData_ReturnsTheSameValueInLoadFunction() = runTest {
        localRepository.savePassword(PASSWORD)
        whenever(localStorageSource.loadPassword()).doReturn(PASSWORD)

        Assert.assertEquals(PASSWORD, localRepository.loadPassword())
    }

    @Test
    fun savePassword_SavesData_ReturnsNotNotEmptyValue() = runTest {
        localRepository.saveUserName(USER_NAME)
        whenever(localStorageSource.loadUserName()).doReturn(USER_NAME)

        Assert.assertNotEquals("", localRepository.loadPassword())
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}
